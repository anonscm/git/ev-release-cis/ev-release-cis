/*
 * evolvis-release-cis,
 * A module for continuum to release on evolvis
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'evolvis-release-cis'
 * Signature of Elmar Geese, 19 December 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.continuum.release.gforge.phase;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;

public class ContinuumGforgeDataStore {

    private static final AtomicReference<ContinuumGforgeDataStore> instance =
            new AtomicReference<ContinuumGforgeDataStore>();

    private final Map<String, ContinuumGforgeData> data;

    public ContinuumGforgeDataStore() {
        this.data = new ConcurrentHashMap<String, ContinuumGforgeData>();
    }

    public void addContinuumData(final ContinuumGforgeData gforgeData) {
        data.put(gforgeData.getProject(), gforgeData);
    }

    public ContinuumGforgeData getContinuumGforgeData(final String projectName) {
        return data.get(projectName);
    }

    public void removeContinuumGforgeData(final ContinuumGforgeData d) {
        if (d != null && d.getProject() != null) {
            data.remove(d.getProject());
        }
    }

    public static ContinuumGforgeDataStore getInstance() {
        // check & re-check cc-pattern to avoid re-reading the config again and
        // again ...
        if (instance.get() != null) {
            return instance.get();
        }
        instance.compareAndSet(null, new ContinuumGforgeDataStore());
        return instance.get();
    }

}
