/*
 * evolvis-release-cis,
 * A module for continuum to release on evolvis
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'evolvis-release-cis'
 * Signature of Elmar Geese, 19 December 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.continuum.release.gforge.webservice;

import org.apache.axis.client.Call;
import org.apache.axis.client.Stub;
import org.apache.log4j.Logger;

import de.tarent.continuum.release.gforge.config.ConfigReader;
import de.tarent.maven.plugins.GForgeLib;

/**
 * 
 * Just a little wrapper to add HTTP Auth support.
 * 
 * TODO: this should be done in the evolvis-ws-api project ...
 * 
 * @author Fabian Bieker
 * 
 */
public class HttpAuthGForgeLib extends GForgeLib {

    public static final String HTTP_AUTH_PROP = "httpAuth";

    private static final Logger log = Logger.getLogger(HttpAuthGForgeLib.class);

    public HttpAuthGForgeLib(String forgeAPIPort_address) {
        super(forgeAPIPort_address);
        configureHttpAuth((Stub) getProxy().getGForgeAPIPortType());
    }

    public static void configureHttpAuth(Stub stub) {

        final ConfigReader config = ConfigReader.getInstance();

        if (!config.hasConfig(HTTP_AUTH_PROP)) {
            log.debug("not setting HTTP Auth - " + HTTP_AUTH_PROP
                    + " property not set");
            return;
        }

        final String val = config.getConfig(HTTP_AUTH_PROP);
        if (val.equalsIgnoreCase("true") || val.equalsIgnoreCase("yes")) {
            final String user = config.getConfig("user");
            final String pass = config.getConfig("pwd");
            log.debug("using '" + user + "' / '***' for HTTP Auth");
            stub._setProperty(Call.USERNAME_PROPERTY, user);
            stub._setProperty(Call.PASSWORD_PROPERTY, pass);
        } else {
            log.debug("not setting HTTP Auth " + HTTP_AUTH_PROP
                    + " property is to " + val);
        }

    }
}
