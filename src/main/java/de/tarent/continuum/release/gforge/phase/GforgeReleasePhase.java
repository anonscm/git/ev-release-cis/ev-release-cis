/*
 * evolvis-release-cis,
 * A module for continuum to release on evolvis
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'evolvis-release-cis'
 * Signature of Elmar Geese, 19 December 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.continuum.release.gforge.phase;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.maven.model.Plugin;
import org.apache.maven.project.MavenProject;
import org.apache.maven.settings.Settings;
import org.apache.maven.shared.release.ReleaseExecutionException;
import org.apache.maven.shared.release.ReleaseFailureException;
import org.apache.maven.shared.release.ReleaseResult;
import org.apache.maven.shared.release.config.ReleaseDescriptor;
import org.apache.maven.shared.release.phase.AbstractReleasePhase;
import org.codehaus.plexus.util.xml.Xpp3Dom;

import de.tarent.continuum.release.gforge.config.ConfigReader;
import de.tarent.continuum.release.gforge.webservice.HttpAuthGForgeLib;
import de.tarent.maven.plugins.GForgeLib;

public class GforgeReleasePhase extends AbstractReleasePhase {

    private static final String MVN_GFORGE_PLUGIN_NAME = "mvn-gforge-plugin";

    private static final Logger log =
            Logger.getLogger(GforgeReleasePhase.class);

    private static final List<String> fileTypes = new ArrayList<String>();

    static {
        fileTypes.add("");
        fileTypes.add("javadoc");
        fileTypes.add("sources");
    }

    public ReleaseResult execute(final ReleaseDescriptor releaseDescriptor,
            final Settings settings, final List reactorProjects)
            throws ReleaseExecutionException, ReleaseFailureException {

        // Note: lib.foo() calls tend to eat exceptions and return strange
        // values.
        // I tried to handle a few known "bad" return values and raise an
        // exception.

        if (log.isInfoEnabled()) {
            log.info("execute(" + releaseDescriptor + ", " + settings + ", "
                    + reactorProjects + ")");
        }

        final ReleaseResult result = new ReleaseResult();
        result.setStartTime(System.currentTimeMillis());

        try {

            result.appendOutput("Starting Gforge-Release");
            log.info("--- Starting Gforge-Release ---");

            final String workingDirectory =
                    releaseDescriptor.getCheckoutDirectory();
            log.info("workingDirectory = " + workingDirectory);
            final MavenProject project =
                    (MavenProject) (reactorProjects.get(0));
            log.info("MavenProject " + project);
            final String projectName = getGforgeProjectName(project);
            log.info("projectName: " + projectName);

            // setup webservice client
            final ContinuumGforgeData gforgeData =
                    ContinuumGforgeDataStore.getInstance().getContinuumGforgeData(
                            project.getArtifactId());
            log.info("gforgeData: " + gforgeData);
            final String gforgeUser = gforgeData.getUsername();
            final String gforgePwd = gforgeData.getPwd();
            final GForgeLib lib =
                    new HttpAuthGForgeLib(ConfigReader.getInstance().getConfig(
                            "GFUrl"));
            log.info("lib: " + lib);
            final String session =
                    lib.createGForgeSession(gforgeUser, gforgePwd);
            result.appendOutput("Created Gforge-Session: " + session);
            log.debug("Created Gforge-Session: " + session + "\n");
            assertTrue(session != null,
                    "Failed to open a session for the GForge Webservice");

            // get gforge userId
            final int gforgeUserId = lib.getGForgeUserID(session, gforgeUser);
            result.appendOutput("Gforge-user-ID for user " + gforgeUser + ": "
                    + gforgeUserId);
            log.debug("Gforge-user-ID for user " + gforgeUser + ": "
                    + gforgeUserId + "\n");
            assertTrue(gforgeUserId != -1, "Failed to get GForge user id for '"
                    + gforgeUser + "' (unkown user?)");

            // get gforge projectId
            final int gforgeProjectID =
                    lib.getGForgeProjectID(session, projectName, gforgeUserId);
            result.appendOutput("Gforge-project-ID for project " + projectName
                    + "(" + gforgeUser + "): " + gforgeProjectID + " \n");
            log.debug("Gforge-project-ID for project " + projectName + "("
                    + gforgeUser + "): " + gforgeProjectID + " \n");
            assertTrue(gforgeProjectID != -1,
                    "Failed to get GForge project id for '" + projectName
                            + "' (unkown project?)");

            final String directory = workingDirectory + "/target";
            result.appendOutput("Working-directory: " + directory);
            log.debug("Working-directory: " + directory);
            final String releaseVersion =
                    releaseDescriptor.getReleaseVersions().values().iterator().next().toString();
            result.appendOutput("ReleaseVersion: " + releaseVersion);
            log.debug("ReleaseVersion: " + releaseVersion);

            final String packageName = project.getArtifactId();

            // upload release files to gforge
            log.debug("UPLOADING FILES WITH PARAMETERS: \n session: " + session
                    + "\nfileType: " + fileTypes + "\ndirectory: " + directory
                    + "\nreleaseVersion: " + releaseVersion + "\npackageName: "
                    + packageName + "\ngforgeProjectID: " + gforgeProjectID);
            // TODO: do not hardcode "i386"
            lib.uploadFilesToGForge(session, fileTypes,
                    getCustomFilePrefix(project), directory, releaseVersion,
                    packageName, gforgeProjectID, "", "", "i386");
            result.appendOutput("Closing Gforge-Session");
            lib.closeGForgeSession(session);

            log.info("Errors GforgeRelease:\n" + lib.getErrorLog() + "\n");
            log.info("Logs GforgeRelease:\n" + lib.getLog() + "\n");
            result.appendOutput("Gforge-Session closed\nGforge Release successfull");
            result.setResultCode(ReleaseResult.SUCCESS);
            log.info("--- Gforge Release successfull ---");

        } catch (final Exception e) {
            // NOTE: setting the result code to ERROR does not help, return
            // value gets not checked by calling function in
            // maven-release-manager 1.0-alpha3

            // result.setResultCode(ReleaseResult.ERROR);
            // result.appendError("Exception while doing Gforge Release", e);

            log.error("Exception while doing Gforge Release", e);
            if (e instanceof ReleaseExecutionException) {
                throw (ReleaseExecutionException) e;
            } else if (e instanceof ReleaseFailureException) {
                throw (ReleaseFailureException) e;
            }
            throw new ReleaseExecutionException(
                    "Failure while running Gforge Release Phase", e);
        } finally {
            result.setEndTime(System.currentTimeMillis());
        }
        return result;
    }

    /**
     * If param <code>cond</code> is false throw an exception, else do nothing.
     */
    private void assertTrue(final boolean cond, final String errorMsg)
            throws ReleaseExecutionException {
        if (!cond) {
            throw new ReleaseExecutionException(errorMsg);
        }
    }

    @SuppressWarnings("unchecked")
    private String getGforgeProjectName(final MavenProject project) {
        String name = project.getArtifactId();
        final List<Plugin> plugis = project.getBuildPlugins();
        for (final Plugin plugin : plugis) {
            if (plugin.getArtifactId().equalsIgnoreCase(MVN_GFORGE_PLUGIN_NAME)) {
                final Xpp3Dom config = (Xpp3Dom) plugin.getConfiguration();
                final Xpp3Dom configProjectName =
                        config.getChild("gForgeProject");
                if (configProjectName != null) {
                    name = configProjectName.getValue();
                }
            }
        }
        return name;
    }

    /**
     * Need to read the pom.xml misself here, since where are not in a maven mojo ... 
     * 
     * This reads the "filePrefix" config setting
     */
    @SuppressWarnings("unchecked")
    private String getCustomFilePrefix(final MavenProject project) {
        final List<Plugin> plugis = project.getBuildPlugins();
        for (final Plugin plugin : plugis) {
            if (plugin.getArtifactId().equalsIgnoreCase(MVN_GFORGE_PLUGIN_NAME)) {
                final Xpp3Dom config = (Xpp3Dom) plugin.getConfiguration();
                final Xpp3Dom configFilePrefix = config.getChild("filePrefix");
                if (configFilePrefix != null) {
                    log.debug("using costum-file prefix '"
                            + configFilePrefix.getValue() + "'");
                    return configFilePrefix.getValue();
                }
            }
        }
        return project.getArtifactId();

    }

    public ReleaseResult simulate(final ReleaseDescriptor releaseDescriptor,
            final Settings settings, final List reactorProjects)
            throws ReleaseExecutionException, ReleaseFailureException {

        if (log.isInfoEnabled()) {
            log.info("simulate(" + releaseDescriptor + ", " + settings + ", "
                    + reactorProjects + ")");
        }

        final ReleaseResult res = new ReleaseResult();
        res.setStartTime(System.currentTimeMillis());

        // TODO: actually simulate the excute(...) call

        res.setEndTime(System.currentTimeMillis());
        return res;
    }

    // TODO: remove?
    public static void main(final String[] args) {
        try {
            new GforgeReleasePhase().execute(new ContinuumReleaseDiscriptor(),
                    new Settings(), new ArrayList<Object>());
        } catch (final ReleaseExecutionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (final ReleaseFailureException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
