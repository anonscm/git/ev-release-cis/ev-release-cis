/*
 * evolvis-release-cis,
 * A module for continuum to release on evolvis
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'evolvis-release-cis'
 * Signature of Elmar Geese, 19 December 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.continuum.release.gforge.phase;

import org.apache.maven.shared.release.config.ReleaseDescriptor;

public class ContinuumReleaseDiscriptor extends ReleaseDescriptor {

    private static final long serialVersionUID = -4406622177649880184L;

    private String gforgeUser;
    private String gforgePwd;

    public String getGforgePwd() {
        return gforgePwd;
    }

    public void setGforgePwd(final String gforgePwd) {
        this.gforgePwd = gforgePwd;
    }

    public String getGforgeUser() {
        return gforgeUser;
    }

    public void setGforgeUser(final String gforgeUser) {
        this.gforgeUser = gforgeUser;
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        } else if (obj instanceof ContinuumReleaseDiscriptor) {
            ContinuumReleaseDiscriptor descriptor =
                    (ContinuumReleaseDiscriptor) obj;
            if ((gforgePwd == null && descriptor.gforgePwd != null)
                    || (gforgeUser == null && descriptor.gforgeUser != null)) {
                return false;
            }
            // Note: superclass overwrite equals, too
            return super.equals(obj)
                    && gforgeUser.equals(descriptor.getGforgeUser())
                    && gforgePwd.equals(descriptor.getGforgePwd());
        }
        return false;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "[ user = " + gforgeUser
                + " , pwd = *** , ...]";
    }

}
