/**
 * 
 */
package de.tarent.continuum.release.gforge.phase;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.apache.maven.settings.Settings;
import org.apache.maven.shared.release.ReleaseExecutionException;
import org.apache.maven.shared.release.ReleaseResult;
import org.junit.BeforeClass;
import org.junit.Test;

import de.tarent.continuum.release.gforge.config.ConfigReader;

/**
 * @author Fabian Bieker
 * 
 */
public class GforgeReleasePhaseTest {

    @BeforeClass
    public static void setEnv() {
        if (System.getenv(ConfigReader.ENV_PLEXUS_HOME) == null
                && System.getProperty(ConfigReader.ENV_PLEXUS_HOME) == null) {
            // TODO: this will not work always ...
            // maybe change ConfigReader to try cp resources, too?
            System.setProperty(ConfigReader.ENV_PLEXUS_HOME,
                    "src/test/resources");
        }
    }

    /**
     * Test method for
     * {@link de.tarent.continuum.release.gforge.phase.GforgeReleasePhase#execute(org.apache.maven.shared.release.config.ReleaseDescriptor, org.apache.maven.settings.Settings, java.util.List)}
     * .
     */
    @Test(expected=ReleaseExecutionException.class)
    public void testExecuteNullArgs() throws Exception {
        final GforgeReleasePhase rel = new GforgeReleasePhase();

        // just try null values ...
        long startTime = System.currentTimeMillis();
        ReleaseResult res = rel.execute(null, null, null);
        assertNotNull(res);
        validateResultTimestamps(startTime, res);
        assertNotSame(ReleaseResult.SUCCESS, res.getResultCode());
    }

    @Test(expected=ReleaseExecutionException.class)
    public void testExecuteInvalid() throws Exception {
        final GforgeReleasePhase rel = new GforgeReleasePhase();
        
        // just try a fake run ...
        long startTime = System.currentTimeMillis();
        ReleaseResult res = rel.execute(new ContinuumReleaseDiscriptor(), new Settings(),
                new ArrayList<Object>());
        assertNotNull(res);
        validateResultTimestamps(startTime, res);
        assertNotSame(ReleaseResult.SUCCESS, res.getResultCode());

    }

   // TODO: try a successful build ...
    
    private void validateResultTimestamps(final long startTime,
            final ReleaseResult res) {
        assertTrue(res.getStartTime() >= startTime);
        assertTrue(res.getStartTime() <= System.currentTimeMillis());
        assertTrue(res.getEndTime() <= System.currentTimeMillis());
        assertTrue(res.getStartTime() <= res.getEndTime());
    }

    /**
     * Test method for
     * {@link de.tarent.continuum.release.gforge.phase.GforgeReleasePhase#simulate(org.apache.maven.shared.release.config.ReleaseDescriptor, org.apache.maven.settings.Settings, java.util.List)}
     * .
     */
    @Test
    public void testSimulate() throws Exception {
        final GforgeReleasePhase rel = new GforgeReleasePhase();

        // just try null values ...
        long startTime = System.currentTimeMillis();
        ReleaseResult res = rel.simulate(null, null, null);
        assertNotNull(res);
        validateResultTimestamps(startTime, res);
        // assertNotSame(ReleaseResult.SUCCESS, res.getResultCode());

        // just try a fake run ...
        startTime = System.currentTimeMillis();
        res = rel.simulate(new ContinuumReleaseDiscriptor(), new Settings(),
                new ArrayList<Object>());
        assertNotNull(res);
        validateResultTimestamps(startTime, res);
        // assertNotSame(ReleaseResult.SUCCESS, res.getResultCode());
        // TODO: more testing ...
    }

}
